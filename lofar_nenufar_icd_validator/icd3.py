from .validator import Validate, check_attributes
from ._types import _group
from ._const import SYS_LOG_GROUP, PROCESS_HISTORY_GROUP, COORDINATE_ATTR, TIME_COORD_GROUP, SPECTRAL_COORD_GROUP, \
    SUB_ARRAY_POINTING_ATTR, FILETYPE_ICD, BEAM_ATTR, ROOT_ICD3_ATTR, STOKES_ATTR, LOGGING_LEVEL
import logging
logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)

logging.basicConfig(level=LOGGING_LEVEL)

ICD_DOC_URL = "https://www.astron.nl/lofarwiki/lib/exe/fetch.php?media=public:documents:lofar-usg-icd-003.pdf"

DATA_GROUP = _group('bfData', STOKES_ATTR, {}, 1)

COORDINATE_GROUP = _group(
    'Coordinate',
    COORDINATE_ATTR,
    {
        'TIME': TIME_COORD_GROUP,
        'SPECTRAL': SPECTRAL_COORD_GROUP
    },
    0)

BEAM_GROUP = _group(
    'Beam',
    BEAM_ATTR,
    {
        'PROCESS_HISTORY': PROCESS_HISTORY_GROUP,
        'STOKES': DATA_GROUP,
        'COORDINATES': COORDINATE_GROUP
    },
    3)


SUB_ARRAY_POINTING_GROUP = _group(
    'SubArrayPointing',
    SUB_ARRAY_POINTING_ATTR,
    {
        'BEAM': BEAM_GROUP,
        'PROCESS_HISTORY': PROCESS_HISTORY_GROUP
    },
    3)

ICD3_GROUP = _group(
    'Root',
    ROOT_ICD3_ATTR,
    {
        'SYS_LOG': SYS_LOG_GROUP,
        'SUB_ARRAY_POINTING': SUB_ARRAY_POINTING_GROUP,
    },
    0)

GROUP_TYPES_ICD3 = {
    "/": ('Root', 'SysLog', 'SubArrayPointing'),
    "SubArrayPointing": ('Beam', 'ProcessHistory', 'Coordinates'),
    "Beam": ('ProcessHistory', 'bfData', 'Coordinates'),
    "Coordinates": ('TimeCoord', 'SpectralCoord'),
}


class ValidateICD3(Validate):

    def __init__(self, h5file):
        Validate.__init__(self, h5file, icd=3, doc_url=ICD_DOC_URL)

    def _validate_icd(self):

        self._validate_root()

        logger.debug('Checking ICD3 Attributes:')
        if self.hdl.attrs['FILETYPE'] != FILETYPE_ICD[f'ICD{self.icd}']:
            logger.warning(f'  Attribute FILETYPE type should be {FILETYPE_ICD[f"ICD{self.icd}"]}. ' +
                           f'It is: "{self.hdl.attrs["FILETYPE"]}"')
        check_attributes(self.hdl, ICD3_GROUP.Attributes, f'ICD{self.icd}')

        for group_path, group_data in ICD3_GROUP.Groups.items():
            self._check_group(f'/{group_path}', group_data)

