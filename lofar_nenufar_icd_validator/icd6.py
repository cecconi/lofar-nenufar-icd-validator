from .validator import Validate, check_attributes
from ._types import _group, _ints, _strings, _array_int_items, _floats, _attribute, _attribute_array
from ._const import SYS_LOG_GROUP, PROCESS_HISTORY_GROUP, COORDINATE_ATTR, TIME_COORD_GROUP, SPECTRAL_COORD_GROUP, \
    FILETYPE_ICD, ROOT_ICD6_ATTR, POLARIZATION_COORD_GROUP, DYN_SPEC_ATTR, LOGGING_LEVEL
import logging
logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)

ICD_DOC_URL = "https://www.astron.nl/lofarwiki/lib/exe/fetch.php?media=public:documents:lofar-usg-icd-006.pdf"

DATA_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('Data',)),
    'WCSINFO': _attribute(_strings, ('/Coordinates',)),
    'DATASET_NOF_AXES': _attribute(_ints, None),
    'DATASET_SHAPE': _attribute_array(_array_int_items, None)
}

DATA_GROUP = _group('Data', DATA_ATTR, {}, 0)

EVENT_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('Event',)),
    'DATASET': _attribute(_strings, ('Event List',)),
    'N_AXIS': _attribute(_ints, (2,)),
    'N_AXIS_1': _attribute(_strings, ('Fields',)),
    'N_AXIS_2': _attribute(_strings, ('Event',)),
    'N_SOURCE': _attribute(_ints, None),
    'FIELD_1': _attribute(_floats, None),
    'FIELD_2': _attribute(_floats, None),
    'FIELD_3': _attribute(_floats, None),
    'FIELD_4': _attribute(_floats, None),
    'FIELD_5': _attribute(_floats, None),
    'FIELD_6': _attribute(_floats, None),
}

EVENT_GROUP = _group('Event', EVENT_ATTR, {}, 0)

COORDINATE_GROUP = _group(
    'Coordinate',
    COORDINATE_ATTR,
    {
        'TIME_COORD': TIME_COORD_GROUP,
        'SPECTRAL_COORD': SPECTRAL_COORD_GROUP,
        'POLARIZATION_COORD': POLARIZATION_COORD_GROUP,
    },
    0)

DYN_SPEC_GROUP = _group(
    'DYN_SPEC',
    DYN_SPEC_ATTR,
    {
        'COORDINATES': COORDINATE_GROUP,
        'DATA': DATA_GROUP,
        'EVENT': EVENT_GROUP,
        'PROCESS_HISTORY': PROCESS_HISTORY_GROUP,
    },
    3
)

ICD6_GROUP = _group(
    'Root',
    ROOT_ICD6_ATTR,
    {
        'SYS_LOG': SYS_LOG_GROUP,
        'TILED_DYN_SPEC': DYN_SPEC_GROUP,
        'DYN_SPEC': DYN_SPEC_GROUP,
    },
    0)

GROUP_TYPES_ICD6 = {
    "/": ('Root', 'SysLog', 'SubArrayPointing'),
    "SubArrayPointing": ('Beam', 'ProcessHistory', 'Coordinates'),
    "Beam": ('ProcessHistory', 'bfData', 'Coordinates'),
    "Coordinates": ('TimeCoord', 'SpectralCoord'),
}


class ValidateICD6(Validate):

    def __init__(self, h5file):
        Validate.__init__(self, h5file, icd=6, doc_url=ICD_DOC_URL)

    def _validate_icd(self):

        self._validate_root()

        logger.debug('Checking ICD6 Attributes:')
        if self.hdl.attrs['FILETYPE'] != FILETYPE_ICD[f'ICD{self.icd}']:
            logger.warning(f'  Attribute FILETYPE type should be {FILETYPE_ICD[f"ICD{self.icd}"]}. ' +
                           f'It is: "{self.hdl.attrs["FILETYPE"]}"')
        check_attributes(self.hdl, ICD6_GROUP.Attributes, f'ICD{self.icd}')

        for group_path, group_data in ICD6_GROUP.Groups.items():
            self._check_group(f'/{group_path}', group_data)
