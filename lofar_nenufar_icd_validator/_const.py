from ._types import _strings, _ints, _bools, _arrays, _floats, _group, \
    _array_string_items, _array_float_items, _array_int_items, _attribute, _attribute_array
import logging

LOGGING_LEVEL = logging.DEBUG

# Common LOFAR Attributes at Root level:
ROOT_CL_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('Root',)),
    'FILETYPE': _attribute(_strings, None),
    'FILENAME': _attribute(_strings, None),
    'FILEDATE': _attribute(_strings, None),
    'TELESCOPE': _attribute(_strings, ('LOFAR',)),
    'OBSERVER': _attribute(_strings, None),
    'PROJECT_ID': _attribute(_strings, None),
    'PROJECT_TITLE': _attribute(_strings, None),
    'PROJECT_CO_I': _attribute(_strings, None),
    'PROJECT_CONTACT': _attribute(_strings, None),
    'OBSERVATION_ID': _attribute(_strings, None),
    'OBSERVATION_START_MJD': _attribute(_floats, None),
    'OBSERVATION_START_TAI': _attribute(_strings, None),
    'OBSERVATION_START_UTC': _attribute(_strings, None),
    'OBSERVATION_END_MJD': _attribute(_floats, None),
    'OBSERVATION_END_TAI': _attribute(_strings, None),
    'OBSERVATION_END_UTC': _attribute(_strings, None),
    'OBSERVATION_NOF_STATIONS': _attribute(_ints, None),
    'OBSERVATION_STATIONS_LIST': _attribute_array(_array_string_items, None),
    'OBSERVATION_FREQUENCY_MAX': _attribute(_floats, None),
    'OBSERVATION_FREQUENCY_MIN': _attribute(_floats, None),
    'OBSERVATION_FREQUENCY_CENTER': _attribute(_floats, None),
    'OBSERVATION_FREQUENCY_UNIT': _attribute(_strings, ('MHz',)),
    'OBSERVATION_NOF_BITS_PER_SAMPLE': _attribute(_ints, None),
    'CLOCK_FREQUENCY': _attribute(_floats, (160., 200.)),
    'CLOCK_FREQUENCY_UNIT': _attribute(_strings, ('MHz',)),
    'ANTENNA_SET': _attribute(_strings, (
        'LBA_INNER', 'LBA_OUTER', 'LBA_SPARSE_EVEN', 'LBA_SPARSE_ODD',
        'LBA_X', 'LBA_Y', 'HBA_ZERO', 'HBA_ONE', 'HBA_DUAL', 'HBA_JOINED'
    )),
    'FILTER_SELECTION': _attribute(_strings, (
        'LBA_10_70', 'LBA_30_70', 'LBA_10_90', 'LBA_30_90',
        'HBA_110_190', 'HBA_170_230', 'HBA_210_250'
    )),
    'TARGET': _attribute(_strings, None),
    'SYSTEM_VERSION': _attribute(_strings, None),
    'PIPELINE_NAME': _attribute(_strings, None),
    'PIPELINE_VERSION': _attribute(_strings, None),
    'ICD_NUMBER': _attribute(_strings, None),
    'ICD_VERSION': _attribute(_strings, None),
    'NOTES': _attribute(_strings, None),
}

# Additional ICD3 Attributes at Root level:
ROOT_ICD3_ATTR = {
    'CREATE_OFFLINE_ONLINE': _attribute(_strings, None),
    'BF_FORMAT': _attribute(_strings, None),
    'BF_VERSION': _attribute(_strings, None),
    'EXPTIME_START_UTC': _attribute(_strings, None),
    'EXPTIME_STOP_UTC': _attribute(_strings, None),
    'EXPTIME_START_MJD': _attribute(_floats, None),
    'EXPTIME_STOP_MJD': _attribute(_floats, None),
    'EXPTIME_START_TAI': _attribute(_strings, None),
    'EXPTIME_STOP_TAI': _attribute(_strings, None),
    'TOTAL_INTEGRATION_TIME': _attribute(_floats, None),
    'OBSERVATION_DATATYPE': _attribute(_strings, None),
    'SUB_ARRAY_POINTING_DIAMETER': _attribute(_floats, None),
    'BANDWIDTH': _attribute(_floats, None),
    'BEAM_DIAMETER': _attribute(_floats, None),
    'WEATHER_TEMPERATURE': _attribute_array(_array_float_items, None),
    'WEATHER_HUMIDITY': _attribute_array(_array_float_items, None),
    'SYSTEM_TEMPERATURE': _attribute_array(_array_float_items, None),
    'NOF_SUB_ARRAY_POINTINGS': _attribute(_ints, None),
}

# Additional ICD6 Attributes at Root level:
ROOT_ICD6_ATTR = {
    'DYN_SPEC_GROUPS': _attribute(_ints, None),
    'NOF_DYN_SPEC': _attribute(_ints, None),
    'CREATE_OFFLINE_ONLINE': _attribute(_strings, None),
    'BF_FORMAT': _attribute(_strings, None),
    'BF_VERSION': _attribute(_strings, None),
    'NOF_STATIONS': _attribute(_ints, None),
    'STATIONS_LIST': _attribute_array(_array_string_items, None),
    'PRIMARY_POINTING_DIAMETER': _attribute(_floats, None),
    'POINT_RA': _attribute(_floats, None),
    'POINT_DEC': _attribute(_floats, None),
    'POINT_ALTITUDE': _attribute_array(_array_float_items, None),
    'POINT_AZIMUTH': _attribute_array(_array_float_items, None),
    'CLOCK_RATE': _attribute(_floats, None),
    'CLOCK_RATE_UNIT': _attribute(_strings, ('MHz',)),
    'NOF_SAMPLES': _attribute(_ints, None),
    'SAMPLING_RATE': _attribute(_floats, None),
    'SAMPLING_RATE_UNIT': _attribute(_strings, ('MHz',)),
    'SAMPLING_TIME': _attribute(_floats, None),
    'SAMPLING_TIME_UNIT': _attribute(_strings, ('µs',)),
    'TOTAL_INTEGRATION_TIME': _attribute(_floats, None),
    'TOTAL_INTEGRATION_TIME_UNIT': _attribute(_strings, ('s',)),
    'SUBBAND_WIDTH': _attribute(_floats, None),
    'SUBBAND_WIDTH_UNIT': _attribute(_strings, ('MHz',)),
    'CHANNEL_WIDTH': _attribute(_floats, None),
    'CHANNEL_WIDTH_UNIT': _attribute(_strings, ('MHz',)),
    'TOTAL_BANDWIDTH': _attribute(_floats, None),
    'WEATHER_TEMPERATURE': _attribute_array(_array_float_items, None),
    'WEATHER_HUMIDITY': _attribute_array(_array_float_items, None),
    'SYSTEM_TEMPERATURE': _attribute_array(_array_float_items, None),
}

FILETYPE_ICD = {
    'ICD1': 'tbb',
    'ICD3': 'bf',
    'ICD5': 'sky',
    'ICD6': 'dynspec',
    'ICD7': 'uv',
    'ICD8': 'rm'
    # other allowed values: 'nfi', 'inst', 'lsm'
}

# SYS_LOG Attributes (TBC)
SYS_LOG_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('SysLog',)),
}

# SUB_ARRAY_POINTING Attributes
SUB_ARRAY_POINTING_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('SubArrayPointing',)),
    'POINT_RA': _attribute(_floats, None),
    'POINT_DEC': _attribute(_floats, None),
    'POINT_ALTITUDE': _attribute_array(_array_float_items, None),
    'POINT_AZIMUTH': _attribute_array(_array_float_items, None),
    'NOF_SAMPLES': _attribute(_ints, None),
    'SAMPLING_RATE': _attribute(_floats, None),
    'SAMPLING_RATE_UNIT': _attribute(_strings, ('MHz',)),
    'SAMPLING_TIME': _attribute(_floats, None),
    'SAMPLING_TIME_UNIT': _attribute(_strings, ('s',)),
    'CHANNELS_PER_SUBBAND': _attribute(_ints, None),
    'SUBBAND_WIDTH': _attribute(_floats, None),
    'SUBBAND_WIDTH_UNIT': _attribute(_strings, ('MHz',)),
    'CHANNEL_WIDTH': _attribute(_floats, None),
    'CHANNEL_WIDTH_UNIT': _attribute(_strings, ('MHz',)),
    'NOF_BEAMS': _attribute(_ints, None),
}

DYN_SPEC_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('DYN_SPEC',)),
    'DYN_SPEC_START_MJD': _attribute(_strings, None),
    'DYN_SPEC_STOP_MJD': _attribute(_strings, None),
    'DYN_SPEC_START_UTC': _attribute(_strings, None),
    'DYN_SPEC_STOP_UTC': _attribute(_strings, None),
    'DYN_SPEC_START_TAI': _attribute(_strings, None),
    'DYN_SPEC_STOP_TAI': _attribute(_strings, None),
    'DYN_SPEC_BANDWIDTH': _attribute(_floats, None),
    'BEAM_DIAMETER': _attribute(_floats, None),
    'TRACKING': _attribute(_strings, ('J2000', 'LMN', 'TBD')),
    'TARGET': _attribute_array(_array_string_items, None),
    'ONOFF': _attribute(_strings, None),
    'POINT_RA': _attribute(_floats, None),
    'POINT_DEC': _attribute(_floats, None),
    'POSITION_OFFSET_RA': _attribute(_floats, None),
    'POSITION_OFFSET_DEC': _attribute(_floats, None),
    'BEAM_DIAMETER_RA': _attribute(_floats, None),
    'BEAM_DIAMETER_DEC': _attribute(_floats, None),
    'BEAM_FREQUENCY_MIN': _attribute(_floats, None),
    'BEAM_FREQUENCY_MAX': _attribute(_floats, None),
    'BEAM_FREQUENCY_CENTER': _attribute(_floats, None),
    'BEAM_FREQUENCY_UNIT': _attribute(_strings, ('MHz',)),
    'BEAM_NOF_STATIONS': _attribute(_ints, None),
    'BEAM_STATIONS_LIST': _attribute_array(_array_string_items, None),
    'DEDISPERSION': _attribute(_strings, ('COHERENT', 'INCOHERENT', 'NONE')),
    'DISPERSION_MEASURE': _attribute(_floats, None),
    'DISPERSION_MEASURE_UNIT': _attribute(_strings, ('pc/cm3',)),
    'BARYCENTER': _attribute(_bools, None),
    'STOKES_COMPONENTS': _attribute_array(_array_string_items, None),
    'COMPLEX_VOLTAGE': _attribute(_bools, None),
    'SIGNAL_SUM': _attribute(_strings, ('COHERENT', 'INCOHERENT_SKY', 'INCOHERENT_PRIMARY_POINTING')),
}

PROCESS_HISTORY_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('ProcessHistory',)),
    'OBSERVATION_PARSET': _attribute(_bools, None),
    'OBSERVATION_LOG': _attribute(_bools, None),
    'PRESTO_PARSET': _attribute(_bools, None),
    'PRESTO_LOG': _attribute(_bools, None),
}

BEAM_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('Beam',)),
    'TARGET': _attribute_array(_array_string_items, None),
    'NOF_STATIONS': _attribute(_ints, None),
    'STATIONS_LIST': _attribute_array(_array_string_items, None),
    'TRACKING': _attribute(_strings, None),
    'POINT_RA': _attribute(_floats, None),
    'POINT_DEC': _attribute(_floats, None),
    'POSITION_OFFSET_RA': _attribute(_floats, None),
    'POSITION_OFFSET_DEC': _attribute(_floats, None),
    'BEAM_DIAMETER_RA': _attribute(_floats, None),
    'BEAM_DIAMETER_DEC': _attribute(_floats, None),
    'BEAM_FREQUENCY_CENTER': _attribute(_floats, None),
    'BEAM_FREQUENCY_CENTER_UNIT': _attribute(_strings, ('MHz',)),
    'FOLDED_DATA': _attribute(_bools, None),
    'FOLD_PERIOD': _attribute(_floats, None),
    'FOLD_PERIOD_UNIT': _attribute(_strings, ('s',)),
    'DEDISPERSION': _attribute(_strings, ('COHERENT', 'INCOHERENT', 'NONE')),
    'DISPERSION_MEASURE': _attribute(_floats, None),
    'DISPERSION_MEASURE_UNIT': _attribute(_strings, ('pc/cm3',)),
    'BARYCENTER': _attribute(_bools, None),
    'NOF_STOKES': _attribute(_ints, (1, 4)),
    'STOKES_COMPONENTS': _attribute_array(_array_string_items, None),
    'COMPLEX_VOLTAGE': _attribute(_bools, None),
    'SIGNAL_SUM': _attribute(_strings, ('COHERENT', 'INCOHERENT')),
}

STOKES_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('bfData',)),
    'DATATYPE': _attribute(_strings, ('char', 'int', 'float', 'double')),
    'STOKES_COMPONENT': _attribute(_strings, None),
    'NOF_SAMPLES': _attribute(_ints, None),
    'NOF_SUBBANDS': _attribute(_ints, None),
    'NOF_CHANNELS': _attribute_array(_array_int_items, None),
}

COORDINATE_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('Coordinates',)),
    'REF_LOCATION_VALUE': _attribute_array(_array_float_items, None),
    'REF_LOCATION_UNIT': _attribute_array(_array_string_items, None),
    'REF_LOCATION_FRAME': _attribute(_strings, (
        'GEOCENTER', 'BARYCENTER', 'HELIOCENTER', 'TOPOCENTER',
        'LSRK', 'LSRD', 'GALACTIC', 'LOCAL_GROUP', 'RELOCATABLE'
    )),
    'REF_TIME_VALUE': _attribute(_floats, None),
    'REF_TIME_UNIT': _attribute(_strings, None),
    'REF_TIME_FRAME': _attribute(_strings, None),
    'NOF_COORDINATES': _attribute(_ints, None),
    'NOF_AXES': _attribute(_ints, None),
    'COORDINATE_TYPES': _attribute_array(_array_string_items, None),
}

TIME_COORD_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('TimeCoord',)),
    'COORDINATE_TYPE': _attribute(_strings, ('Time',)),
    'STORAGE_TYPE': _attribute_array(_array_string_items, (b'Linear', b'Tabular')),
    'NOF_AXES': _attribute(_ints, (1,)),
    'AXIS_NAMES': _attribute_array(_array_string_items, (b'Time',)),
    'AXIS_UNITS': _attribute_array(_array_string_items, ('µs'.encode('utf8'),)),
    'REFERENCE_VALUE': _attribute_array(_array_float_items, None),
    'REFERENCE_PIXEL': _attribute_array(_array_float_items, None),
    'INCREMENT': _attribute_array(_array_float_items, None),
    'PC': _attribute_array(_array_float_items, None),
    'AXIS_VALUES_PIXEL': _attribute_array(_array_float_items, None),
    'AXIS_VALUES_WORLD': _attribute_array(_array_float_items, None),
}

SPECTRAL_COORD_ATTR = {
    'GROUPTYPE': _attribute(_strings, ('SpectralCoord',)),
    'COORDINATE_TYPE': _attribute(_strings, ('Spectral',)),
    'STORAGE_TYPE': _attribute_array(_array_string_items, ('Linear', 'Tabular')),
    'NOF_AXES': _attribute(_ints, (1,)),
    'AXIS_NAMES': _attribute_array(_array_string_items,  ('Frequency',)),
    'AXIS_UNITS': _attribute_array(_array_string_items, ('MHz',)),
    'REFERENCE_VALUE': _attribute_array(_array_float_items, None),
    'REFERENCE_PIXEL': _attribute_array(_array_float_items, None),
    'INCREMENT': _attribute_array(_array_float_items, None),
    'PC': _attribute_array(_array_float_items, None),
    'AXIS_VALUES_PIXEL': _attribute_array(_array_float_items, None),
    'AXIS_VALUES_WORLD': _attribute_array(_array_float_items, None),
}

POLARIZATION_COORD_ATTR = {
    'GROUP_TYPE': _attribute(_strings, ('PolarizationCoord',)),
    'COORDINATE_TYPE': _attribute(_strings, ('Polarization',)),
    'STORAGE_TYPE': _attribute_array(_array_string_items, ('Tabular',)),
    'NOF_AXES': _attribute(_ints, (1,)),
    'AXIS_NAMES': _attribute_array(_array_string_items, ('Polarization',)),
    'AXIS_UNITS': _attribute_array(_array_string_items, ('NONE',)),
}

# Groups

SYS_LOG_GROUP = _group('SysLog', SYS_LOG_ATTR, None, 0)
PROCESS_HISTORY_GROUP = _group('ProcessHistory', PROCESS_HISTORY_ATTR, None, 0)
TIME_COORD_GROUP = _group('Time', TIME_COORD_ATTR, None, 0)
SPECTRAL_COORD_GROUP = _group('SpectralCoord', SPECTRAL_COORD_ATTR, None, 0)
POLARIZATION_COORD_GROUP = _group('PolarizationCoord', POLARIZATION_COORD_ATTR, None, 0)
