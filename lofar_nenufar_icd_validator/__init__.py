from .icd3 import ValidateICD3
from .icd6 import ValidateICD6


def validate(h5file, icd):
    if icd == 3:
        ValidateICD3(h5file)
    elif icd == 6:
        ValidateICD6(h5file)
    else:
        raise NotImplementedError()
