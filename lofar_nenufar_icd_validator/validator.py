import h5py
import os
from pathlib import Path
from ._types import _arrays, _str
from ._const import FILETYPE_ICD, ROOT_CL_ATTR, LOGGING_LEVEL
import logging
logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)


def check_attribute_type(item_name, item, item_type):
    """
    checks type of attribute and returns True if type is valid.

    :param item_name: name of the Attribute
    :param item: value of the Attribute
    :param item_type: specification type for this attribute
    :return: True if attribute has valid type
    :rtype: bool
    """
    if not isinstance(item, item_type):
        logger.warning(f'  Attribute {item_name} type should be {item_type}. It is: {type(item)}')
        return False
    else:
        return True


def check_attributes(f, attr_list, attr_list_name):
    """
    Check attributes for the specified file or group.

    The function first checks if the required attributes are present. For the attributes that are present, it
    checks their type, and their value(s), when applicable.

    :param f: HFD5 file handle or group
    :param attr_list: attribute list to check
    :param attr_list_name: HFD5 group name
    :return:
    """

    for attr_name in attr_list:

        if attr_name not in f.attrs.keys():
            logger.warning(f'  Attribute {attr_name} not present ({attr_list_name})')
        else:

            # The attribute is present, let's check its type:
            type_check = check_attribute_type(attr_name, f.attrs[attr_name], attr_list[attr_name].type)
            if type_check:
                # run this only if type is ok
                if attr_list[attr_name].type == _arrays:
                    # In the case of arrays, item types are checked also:
                    for item in f.attrs[attr_name]:
                        check_attribute_type(f'{attr_name} item', item, attr_list[attr_name].item_type)

                if attr_list[attr_name].values is not None:
                    # If values are specified, let's check those:
                    values = attr_list[attr_name].values
                    if _str(f.attrs[attr_name]) not in values:
                        logger.warning(f'  Attribute {attr_name} should have value(s): {", ".join(values)}. ' +
                                        f'It is: "{_str(f.attrs[attr_name])}"')


def check_group(f, attr_list, h5path):
    """
    Checks if a group is present

    :param f: HDF5 file handle
    :param attr_list: list of attributes to be checked
    :param h5path: path of sub-group to be checked
    :return:
    """
    group_name = h5path.split('/')[-1]
    if group_name not in f.keys():
        logger.warning(f'  Group "{group_name}" should be present')
    else:
        logger.info(f'  Group "{group_name}" is present: ok.')
        check_attributes(f[h5path], attr_list, group_name)


class Validate:

    def __init__(self, h5file, icd, doc_url):
        self.file = Path(h5file)
        self.icd = icd
        self.icd_doc_url = doc_url
        self.hdl = self._open()
        logger.info(f'Checking file: {self.file}')
        self._validate_icd()
        self._close()

    def _open(self):
        try:
            f = h5py.File(self.file, 'r')
        except OSError:
            if os.getenv('HDF5_USE_FILE_LOCKING') in [None, 'TRUE']:
                os.environ['HDF5_USE_FILE_LOCKING'] = 'FALSE'
            f = h5py.File(self.file, 'r')
        return f

    def _close(self):
        self.hdl.close()

    @property
    def icd(self):
        return self._icd

    @icd.setter
    def icd(self, value):

        if f'ICD{value}' not in FILETYPE_ICD.keys():
            raise ValueError(f'ICD{value} unknown')

        if value not in [3, 6]:
            raise NotImplementedError(f'ICD{value} validation not implemented')

        self._icd = value

    def _validate_root(self):
        logger.debug('Checking CLA Attributes:')
        check_attributes(self.hdl, ROOT_CL_ATTR, 'CLA')

    def _validate_icd(self):
        """
        validates a LOFAR HDF5 file against an ICD specification
        :return:
        """
        pass

    def _check_group(self, group_path, group_data):
        """
        Checks group/objects (name, count, attributes and children), recursively

        :param group_path: HDF5 path to check
        :type group_path: str
        :param group_data: reference group data
        :type group_data: _group
        :return:
        """
        logger.debug(f'Checking {group_data.Name}:')
        if group_data.Enum > 0:

            # if several objects are expected, try to get the NOF_{} attribute at parent path level
            parent_path = "/".join(group_path.split("/")[:-1])
            if parent_path == '':
                parent_path = '/'

            cnt_attribute = f'NOF_{group_path.split("/")[-1]}'
            if not cnt_attribute.endswith('S'):
                cnt_attribute = f'{cnt_attribute}S'

            if cnt_attribute in self.hdl[parent_path].attrs.keys():
                group_cnt = self.hdl[parent_path].attrs[cnt_attribute]
            else:
                logger.warning(f'  Missing {cnt_attribute} attribute in {parent_path}. Checking only 1st instance.')
                group_cnt = 1

            # Build list of objects accordingly:
            if group_data.Enum == 1:
                cur_group_path = [f'{group_path}_{i}' for i in range(group_cnt)]
            else:
                cur_group_path = [f'{group_path}_{i:0{group_data.Enum}d}' for i in range(group_cnt)]
        else:
            cur_group_path = [group_path]

        for item in cur_group_path:
            logger.debug(f'  Path: {item}')
            try:
                check_attributes(self.hdl[item], group_data.Attributes, group_data.Name)
                if group_data.Groups is not None:
                    for sub_group_path, sub_group_data in group_data.Groups.items():
                        self._check_group(f'{item}/{sub_group_path}', sub_group_data)
            except KeyError as e:
                logger.error(f"Error: {e}")
                logger.info(f"Available keys: {self.hdl[str(Path(item).parent)].keys()}")
                logger.info(f"Check documentation at: {self.icd_doc_url}")
