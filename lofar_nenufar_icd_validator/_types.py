import h5py
import numpy
from collections import namedtuple

__ALL__ = ['_strings', '_floats', '_ints', '_arrays', '_bools', '_str', '_group',
           '_attribute', '_attribute_array']

_H5PY_MAIN_VERSION = h5py.version.version_tuple[0]

if _H5PY_MAIN_VERSION >= 3:
    _strings = str
else:
    _strings = numpy.bytes_

_floats = numpy.float64
_ints = numpy.int64
_bools = numpy.bool_

_arrays = numpy.ndarray
_array_string_items = numpy.bytes_
_array_int_items = _ints
_array_float_items = _floats


def _str(x):
    if _H5PY_MAIN_VERSION >= 3:
        return x
    else:
        return x.decode('ascii')


_group = namedtuple('Group', ['Name', 'Attributes', 'Groups', 'Enum'])
_attribute = namedtuple('Attribute', ['type', 'values'])
_attribute_array = namedtuple('AttributeArray', ['item_type', 'values'])
_attribute_array.type = _arrays
