# LOFAR-NenuFAR ICD ICD Validator 

The validator currently implements [ICD-003](https://www.astron.nl/lofarwiki/lib/exe/fetch.php?media=public:documents:lofar-usg-icd-003.pdf) (Tide-Array-Beam) and [ICD-006](https://www.astron.nl/lofarwiki/lib/exe/fetch.php?media=public:documents:lofar-usg-icd-006.pdf) (Dynamic-Spectra) models. 

More info in the [LOFAR file formats and interfaces](https://www.astron.nl/lofarwiki/doku.php?id=public:documents:lofar_documents)

## Command Line interface

### Synopsis
```
python lofar-icd-validator.py [ -i icd ] [ -v ] file
```

### Description
**lofar-icd-validator** validates the HDF5 **file** against the selected ICD specification. 

### Options
- **-i icd** <br/> ICD number; the default is 3. 

- **-v**  <br/> Verbose mode flag

### Implementation
The current implementation only allows for ICD 3 or 6 validation. 

## How to run the validator

To valide an ICD-003 file named `l4sw_uk902c_20210408T094403_SUN_bfs_IQUV_icd3.h5`:

```bash
python lofar-icd-validator.py -i 3 -v l4sw_uk902c_20210408T094403_SUN_bfs_IQUV_icd3.h5
```
