#!/usr/bin/env python
import sys
from lofar_nenufar_icd_validator import validate

if __name__ == "__main__":

    args = sys.argv[1:]
    icd = 3  # default ICD value
    verb = False

    if '-i' in args:
        pos = args.index('-i')
        args.remove('-i')
        icd = int(args.pop(pos))
    if '-v' in args:
        args.remove('-v')
        verb = True

    if verb:
        print(f'Validating file against ICD{icd} specification.')

    validate(args[0], icd)
